import { useContext, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Call from '../components/Call/Call';
import Calling from '../components/Call/Calling';
import ErrorModal from '../components/layouts/ErrorModal';
import CallButton from '../components/UI/CallButton';
import CancelButton from '../components/UI/CancelButton';
import createNewSocket from '../store/jssip';
import { resetTimer } from '../store/phoneSlice';
import { time } from '../store/thunks';
import { socketContext } from '../store/SocketProvider';
import './DialNumber.css';

const isValidNumber = (enteredValue) => enteredValue.trim().length === 10;

const DialNumber = (props) => {
	const [isCalling, setIsCalling] = useState(false);
	const [isShowError, setIsShowError] = useState(false);
	const [errorMessage, setErrorMessage] = useState(undefined);
	const [callSession, setCallSession] = useState();
	const [isCountTime, setIsCountTime] = useState(false);
	const socket = useContext(socketContext);
	const [callLog, setCallLog] = useState({});
	const [newCall, setNewCall] = useState(undefined);
	const phoneNumber = useSelector((state) => state.phoneReducer.phoneNumber);
	const dispatch = useDispatch();

	useEffect(() => {
		setNewCall(createNewSocket());

		if (callSession) {
			callSession.connection.addEventListener('addstream', (e) => {
				const audio = document.createElement('audio');
				audio.srcObject = e.stream;
				audio.play();
			});
		}
	}, [callSession]);

	const eventHandlers = {
		progress: (e) => {
			setIsCountTime(false);
			dispatch(resetTimer());
			const callData = {
				caller: e.response.from.uri.user,
				recipient: e.response.to._uri.user,
				status: 'makingCall_start',
			};

			socket.emit('createLog', callData);
			socket.on('createLog', (newLog) => {
				setCallLog(newLog);
			});
		},
		failed: (e) => {
			setErrorMessage(e.cause);
			setCallSession(undefined);
			const dataUpdate = {
				status: e.cause,
			};
			socket.emit('updateLog', { id: callLog._id, dataUpdate });
		},
		ended: (e) => {
			console.log(callLog);
			const dataUpdate = {
				endedBy: callLog.recipient,
			};
			socket.emit('updateLog', { id: callLog._id, dataUpdate });
			setIsCountTime(false);
			setIsCalling(false);
			dispatch(resetTimer());
			clearInterval(time);
		},
		confirmed: (e) => {
			console.log(callLog);
			setIsCountTime(true);
			setCallSession(undefined);
			const dataUpdate = {
				status: 'makingCall_connected',
			};
			socket.emit('updateLog', { id: callLog._id, dataUpdate });
		},
	};

	const closeErrorHandler = () => {
		setIsShowError(false);
	};

	const callHandler = () => {
		if (!isValidNumber(phoneNumber)) {
			setIsShowError(true);
			setErrorMessage('Please enter a valid phone number');
			return;
		}

		newCall.start();
		setIsCalling(true);

		var options = {
			eventHandlers,
			mediaConstraints: { audio: true, video: false },
		};

		const session = newCall.call(phoneNumber, options);
		setCallSession(session);
		setErrorMessage(undefined);
	};

	const endedHandler = () => {
		if (callSession) {
			callSession.terminate();
			const dataUpdate = {
				status: 'makingCall_cancel',
				endedBy: callLog.caller,
			};
			socket.emit('updateLog', { id: callLog._id, dataUpdate });
		}
		setCallSession(undefined);
		setIsCalling(false);
		setErrorMessage(undefined);
	};

	return (
		<main className='dial'>
			{!isCalling && <Call />}
			{!isCalling && <CallButton onClick={callHandler} />}
			{isCalling && <Calling error={errorMessage} timers={isCountTime} />}
			{isCalling && <CancelButton onClick={endedHandler} />}
			<ErrorModal isShow={isShowError} onClose={closeErrorHandler}>
				<p className='text-danger'>{errorMessage}</p>
			</ErrorModal>
		</main>
	);
};

export default DialNumber;
