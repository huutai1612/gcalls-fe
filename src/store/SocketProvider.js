import React, { useState } from 'react';
import { io } from 'socket.io-client';

export const socketContext = React.createContext({});

const SocketProvider = (props) => {
	const [socket] = useState(
		io('http://localhost:4000', {
			transports: ['websocket'],
		}),
	);

	return (
		<socketContext.Provider value={socket}>
			{props.children}
		</socketContext.Provider>
	);
};

export default SocketProvider;
