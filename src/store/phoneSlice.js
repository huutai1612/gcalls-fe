import { createSlice } from '@reduxjs/toolkit';

const convertTime = (time) => `0${time}`.slice(-2);

const initialState = {
	phoneNumber: '',
	seconds: 0,
	minutes: 0,
	duration : '00 : 00',
};

const phoneSlice = createSlice({
	name: 'Phone',
	initialState,
	reducers: {
		addNumberToPhone: (state, action) => {
			if (state.phoneNumber.length === 10) return;
			state.phoneNumber = state.phoneNumber + action.payload.trim();
		},
		removeNumberOfPhone: (state) => {
			state.phoneNumber = state.phoneNumber.slice(0, -1);
		},
		setPhoneNumber: (state, action) => {
			state.phoneNumber = action.payload.trim();
		},
		resetPhoneNumber: (state) => {
			state.phoneNumber = '';
		},
		countTime: (state) => {
			if (state.seconds === 60) {
				state.seconds = 0;
				state.minutes += 1;
			}
			state.seconds += 1;
			state.duration = `${convertTime(state.minutes)} : ${convertTime(state.seconds)}`
		},
		resetTimer: (state) => {
			state.seconds = 0;
			state.minutes = 0;
			state.duration = '00 : 00'
		},
	},
});

export const {
	addNumberToPhone,
	removeNumberOfPhone,
	setPhoneNumber,
	resetPhoneNumber,
	countTime,
	resetTimer,
} = phoneSlice.actions;

export default phoneSlice.reducer;
