import { configureStore } from '@reduxjs/toolkit';
import phoneReducer from './phoneSlice';

const store = configureStore({
	reducer: {
		phoneReducer,
	},
});

export default store;
