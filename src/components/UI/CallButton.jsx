import './CallButton.css';

const CallButton = (props) => {
	return (
		<button className='dial__call-btn' onClick={props.onClick}>
			<i className='fas fa-phone-alt'></i>
		</button>
	);
};

export default CallButton;
