import './CallingButton.css';

const CallingButton = (props) => {
	return <button className='dial__calling-btn'>{props.children}</button>;
};

export default CallingButton;
