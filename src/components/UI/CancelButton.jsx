import './CancelButton.css';

const CancelButton = (props) => {
	return (
		<button className='dial__cancel-btn' onClick={props.onClick}>
			<i className='fas fa-phone-slash'></i>
		</button>
	);
};

export default CancelButton;
