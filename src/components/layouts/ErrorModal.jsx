import { Modal, Button } from 'react-bootstrap';

const ErrorModal = (props) => {
	return (
		<Modal size='lg' show={props.isShow} onHide={props.onClose}>
			<Modal.Header closeButton>
				<Modal.Title>
					<h2 className='text-danger'>Error</h2>
				</Modal.Title>
			</Modal.Header>
			<Modal.Body>{props.children}</Modal.Body>
			<Modal.Footer>
				<Button variant='secondary' onClick={props.onClose}>
					Close
				</Button>
			</Modal.Footer>
		</Modal>
	);
};

export default ErrorModal;
