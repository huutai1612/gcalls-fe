import { Fragment } from 'react';
import NumberBtn from '../UI/NumberBtn';

const NumPad = (props) => {
	return (
		<Fragment>
			<NumberBtn value='1'>1</NumberBtn>
			<NumberBtn value='2'>
				2<span>A B C</span>
			</NumberBtn>
			<NumberBtn value='3'>
				3<span>D E F</span>
			</NumberBtn>
			<NumberBtn value='4'>
				4<span>G H I</span>
			</NumberBtn>
			<NumberBtn value='5'>
				5<span>J K L</span>
			</NumberBtn>
			<NumberBtn value='6'>
				6<span>M N O</span>
			</NumberBtn>
			<NumberBtn value='7'>
				7<span>P Q R S</span>
			</NumberBtn>
			<NumberBtn value='8'>
				8<span>T U V</span>
			</NumberBtn>
			<NumberBtn value='9'>
				9<span>W X Y Z</span>
			</NumberBtn>
			<NumberBtn value='*'>*</NumberBtn>
			<NumberBtn value='0'>
				0 <span>+</span>
			</NumberBtn>
			<NumberBtn value='#'>#</NumberBtn>
		</Fragment>
	);
};

export default NumPad;
