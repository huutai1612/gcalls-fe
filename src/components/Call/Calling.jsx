import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { countTime } from '../../store/phoneSlice';
import { timerCount } from '../../store/thunks';
import CallingButton from '../UI/CallingButton';
import './Calling.css';

const Calling = (props) => {
	const phoneNumber = useSelector((state) => state.phoneReducer.phoneNumber);
	const duration = useSelector((state) => state.phoneReducer.duration)
	const dispatch = useDispatch();

	useEffect(() => {
		if (props.timers) {
			dispatch(timerCount(countTime));
		}
	}, [props.timers, dispatch]);

	return (
		<div className='dial__calling'>
			<h2 className='dial__calling-recipient'>{phoneNumber}</h2>
			<h2 className='dial__calling-brand'>Gcalls</h2>
			<h2 className='dial__calling-status'>
				{props.error
					? props.error
					: props.timers
					? duration
					: 'Calling.....'}
			</h2>
			<div className='dial__calling-container'>
				<CallingButton>
					<i className='fas fa-microphone-slash'></i>
					<p>mute</p>
				</CallingButton>
				<CallingButton>
					<i className='fas fa-pause'></i>
					<p>pause</p>
				</CallingButton>
				<CallingButton>
					<i className='fas fa-phone'></i>
					<p>forward</p>
				</CallingButton>
			</div>
		</div>
	);
};

export default Calling;
